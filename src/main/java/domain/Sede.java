package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity

public class Sede implements BaseEntity<Long> {
	@Id
	@SequenceGenerator(name = "sede_id_generator", sequenceName = "sede_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sede_id_generator")
	private Long id;

	@Column(unique = true, nullable = false, length = 250)
	private String direccion;

	public Sede() {
	}

	public Sede(String direccion) {
		this.direccion = direccion;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Long getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@ManyToOne(mappedBy = "bank")
	private Collection<bank> banks;
}