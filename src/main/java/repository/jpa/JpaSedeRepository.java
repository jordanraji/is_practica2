package repository.jpa;
import java.util.Collection;
import javax.persistence.TypedQuery;
import org.springframework.stereotype.Repository;
import repository.SedeRepository;
import domain.Sede;

@Repository
public class JpaSedeRepository extends JpaBaseRepository<Sede, Long> implements
		SedeRepository {

	@Override
	public Sede findByNumber(String number) {
		String jpaQuery = "SELECT a FROM Sede a WHERE a.number = :number";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("number", number);
		return getFirstResult(query);
	}

	@Override
	public Sede findByDireccion(String direccion) {
		String jpaQuery = "SELECT a FROM Sede a WHERE a.direccion = :direccion";
		TypedQuery<Sede> query = entityManager.createQuery(jpaQuery, Sede.class);
		query.setParameter("direccion", direccion);
		return getFirstResult(query);
	}
}